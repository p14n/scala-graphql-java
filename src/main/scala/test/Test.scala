package test

import java.util

import com.fasterxml.jackson.databind.ObjectMapper
import graphql.execution.batched.BatchedExecutionStrategy
import graphql.language._
import graphql.parser.Parser
import graphql.{GraphQLError, ExecutionResult, GraphQL}
import org.http4s.MediaType._
import org.http4s._
import org.http4s.client.blaze._
import org.http4s.dsl._
import org.http4s.headers._
import org.http4s.server.blaze._

import scala.collection.JavaConversions._
import scala.io.Source

/**
  * Created by p14n on 04/05/2016.
  */
object Test extends App {

  val printer = new GraphQLPrinter()

  def splitDocumentByRootFields(requestString: String): Map[String, Document] = {
    val parser: Parser = new Parser
    val document = parser.parseDocument(requestString)
    var opsByName = Map[String, OperationDefinition]()

    document.getDefinitions.foreach { (definition: Definition) => {
      definition match {
        case op: OperationDefinition => {
          val fieldsByName = op.getSelectionSet.getSelections.foldLeft(Map[String, util.List[Field]]()) { (all, selection) =>
            selection match {
              case f: Field => {
                val name = f.getName
                all.get(name) match {
                  case Some(list) =>
                    list.add(f)
                    all
                  case _ =>
                    val list = new util.ArrayList[Field]()
                    list.add(f)
                    all + (name -> list)
                }
              }
            }
          }
          fieldsByName.foreach { (kv) => {
            val list = new util.ArrayList[Selection]()
            list.addAll(kv._2)
            opsByName = opsByName + (kv._1 -> new OperationDefinition(op.getName, op.getOperation, op.getVariableDefinitions, op.getDirectives, new SelectionSet(list)))
          }
          }
        }
        case _ =>
      }
     }
    }
    opsByName.foldLeft(Map[String, Document]()) { (all, kv) =>
      val doc = new Document()
      doc.getDefinitions.add(kv._2)
      all + (kv._1 -> doc)
    }
  }

  def mergeMap(all: util.HashMap[Any, Any], map: util.Map[Any, Any]):Unit = {
    try {
      for(key <- map.keySet()){
        val allVal = all.get(key)
        val mapVal = map.get(key)
        if(allVal == null) {
          all.put(key, mapVal)
        } else if(mapVal == null){

        } else if(allVal.isInstanceOf[util.List[Any]]){
          val mapList = mapVal.asInstanceOf[util.List[Any]]
          if(!mapList.isEmpty && mapList.get(0).isInstanceOf[util.HashMap[Any,Any]]){
            val mapListOfMaps = mapList.asInstanceOf[util.List[util.HashMap[Any,Any]]]
            val allListOfMaps = allVal.asInstanceOf[util.List[util.HashMap[Any,Any]]]
            val groupedMaps = mapListOfMaps.groupBy{ m =>
              m.getOrElse("name","")
            }
            val groupedAll = allListOfMaps.groupBy{ m =>
              m.getOrElse("name","")
            }
            groupedMaps.foreach { kv =>
              if(groupedAll.containsKey(kv._1)){
                val existing:util.HashMap[Any,Any] = groupedAll(kv._1)(0)
                mergeMap(existing,kv._2(0))
              } else {
                val existing:util.HashMap[Any,Any] = kv._2(0)
                allListOfMaps.add(existing)
              }
            }
          } else {
            allVal.asInstanceOf[util.List[Any]].addAll(mapList)
          }
        } else if(allVal.isInstanceOf[util.Map[Any,Any]]){
          mergeMap(allVal.asInstanceOf[util.HashMap[Any,Any]],mapVal.asInstanceOf[util.HashMap[Any,Any]])
        }
      }
    } catch {
      case e => e.printStackTrace()
    }
  }

  def mergeResults(results: Iterable[ExecutionResult]): ExecutionResult = {
    val errors = results.foldLeft(new util.ArrayList[GraphQLError]()) { (all, result) =>
      all.addAll(result.getErrors)
      all
    }
    val data = results.foldLeft(new util.HashMap[Any,Any]()) { (all, result) =>
      if(result.getData.isInstanceOf[util.Map[_,_]]){
        val map:util.Map[Any, Any] = result.getData.asInstanceOf[util.Map[Any,Any]]
        mergeMap(all,map)
      }
      all
    }
    new ExecutionResult {
      override def getData: AnyRef = data
      override def getErrors: util.List[GraphQLError] = errors
    }
  }

  val service = HttpService {
    case GET -> Root / fileName => {
      val fileContents = Source.fromFile("/Users/p14n/dev/tools/graphiql/example/" + fileName).getLines.mkString("\n")
      val response = Ok(fileContents)
      if (fileName.endsWith("html")) response.putHeaders(`Content-Type`(`text/html`)) else response
    }
    case req@POST -> Root / "graphql" => {
      req.decode[String] { body =>

        val mapper = new ObjectMapper()
        val parsed = mapper.readTree(body)

        println("In " + body)
        /*        val parsed : Option[JSONType] = JSON.parseRaw(body)
                val query = parsed match {
                  case Some(JSONObject(obj)) =>
                    obj("query").toString
                }
                println("Query "+query)*/
        val qText = parsed.get("query").asText()
        println(qText)

        val splitDocs = splitDocumentByRootFields(qText)

        println("Query roots:" + splitDocs.keySet)

        val results:Iterable[ExecutionResult] = if(splitDocs.contains("__schema")) {
          Schema.schema.values.map { schema =>
            val graphQl = new GraphQL(schema, new BatchedExecutionStrategy);
            graphQl.execute(qText)
          }
        } else splitDocs.flatMap { kv =>
            val schema = Schema.schema(kv._1)
            if (schema != null) {
              val splitText = printer.print(kv._2)
              val graphQl = new GraphQL(schema, new BatchedExecutionStrategy);
              val result = graphQl.execute(splitText)

              for (e <- result.getErrors) {
                println("Error " + e)
              }

              Some(result)
            } else None
        }


        val result = mergeResults(results)

        val resultText = if (result.getErrors.isEmpty) {
          val data = new util.HashMap[String, Any]()
          data.put("data", result.getData)
          mapper.writeValueAsString(data)
        } else {
          val data = new util.HashMap[String, Any]()
          data.put("errors", result.getErrors)
          mapper.writeValueAsString(data)
        }
        //val data = result.getData
        println("Out: " + resultText)

        Ok(resultText).putHeaders(`Content-Type`(`application/json`))


      }

    }
  }

  val builder = BlazeBuilder.mountService(service)

  val server = builder.run

  val client = PooledHttp1Client()

  val muppetQuery = client.fetchAs[String](Request(method = POST, uri = uri("http://localhost:8080/graphql")).withBody[String](
    """
      |query MuppetQuery {
      |  person(code: 1) {
      |    name,
      |    address {
      |      city
      |    }
      |  }
      |}
      | """.stripMargin))

  //println(muppetQuery.run);
  client.shutdownNow()
  Thread.currentThread().join()
  server.shutdownNow()


}

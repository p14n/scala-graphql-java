package test

import java.util.{List => JList, Map => JMap}

import graphql.Scalars._
import graphql.execution.batched.Batched
import graphql.language.{Field, SelectionSet}
import graphql.schema.GraphQLArgument._
import graphql.schema.GraphQLFieldDefinition._
import graphql.schema.GraphQLObjectType._
import graphql.schema._

import scala.collection.JavaConversions._

/**
  * Created by p14n on 17/05/2016.
  */
object Schema {

  def fieldNames(ss: SelectionSet): Set[String] = {
    ss.getSelections.flatMap {
      (sel) => sel match {
        case f: Field => Some(f.getName)
        case _ => None
      }
    }.toSet
  }


  lazy val schema = {
    def strField(name: String): GraphQLFieldDefinition.Builder = {
      field(name, GraphQLString)
    }

    def field(name: String, tp: GraphQLOutputType): GraphQLFieldDefinition.Builder = {
      newFieldDefinition().`type`(tp).name(name)
    }

    def selectedFields(environment: DataFetchingEnvironment): Set[String] = {
      if (environment != null && environment.getFields != null && !environment.getFields.isEmpty) {
        fieldNames(environment.getFields.get(0).getSelectionSet)
      } else Set()
    }


    def fetcher[T](f: (DataFetchingEnvironment) => List[T]): DataFetcher = {
      new DataFetcher {
        @Batched override def get(environment: DataFetchingEnvironment): AnyRef = {
          println("Selected Fields " + selectedFields(environment))
          seqAsJavaList(f(environment))
        }
      }
    }
    def arg[T](e: DataFetchingEnvironment, name: String): Option[T] = {
      val result = e.getArgument[T](name)
      if (result == null) None else Some(result)
    }
    def intArg(name: String): GraphQLArgument = {
      newArgument().name(name).`type`(GraphQLInt).build()
    }

    val personAddress = Map(
      "Andrew" -> mapAsJavaMap(Map("city" -> "Milton Keynes", "postcode" -> "MK1 XXX")),
      "David" -> mapAsJavaMap(Map("city" -> "Newcastle", "postcode" -> "NE1 XXX")),
      "Dean" -> mapAsJavaMap(Map("city" -> "Brighton", "postcode" -> "BN1 XXX")))


    val addressFetcher = fetcher[JMap[String, _]]((e: DataFetchingEnvironment) => {
      val values = e.getSource.asInstanceOf[JList[JMap[String, String]]]
      values.toList.map((m) => {
        if (personAddress.containsKey(m.get("name"))) Some(personAddress(m.get("name"))) else None
      }).flatten
    })

    val persons = Map(
      1 -> mapAsJavaMap(Map("name" -> "Andrew")),
      2 -> mapAsJavaMap(Map("name" -> "David")),
      3 -> mapAsJavaMap(Map("name" -> "Dean")))

    val personFetcher = fetcher[JList[JMap[String, _]]]((e: DataFetchingEnvironment) => {
      val code = arg[Int](e, "code")
      code match {
        case None => List(seqAsJavaList(persons.values.toList))
        case Some(x) => List(seqAsJavaList(List(persons(x))))
      }
    })

    val animalFetcher = fetcher[JList[JMap[String, _]]]((e: DataFetchingEnvironment) => {
      List(seqAsJavaList(List(Map("name" -> "Monkey"))))
    })
    val habitatFetcher = fetcher[JMap[String, _]]((e: DataFetchingEnvironment) => {
      List(Map("name" -> "Jungle"))
    })

    val addressType = newObject()
      .name("address")
      .field(strField("city").build())
      .field(strField("postcode").build())
      .build()

    val personType = newObject()
      .name("person")
      .field(strField("name").build())
      .field(field("address", addressType).dataFetcher(addressFetcher).build())
      .build()

    val habitatType = newObject()
      .name("habitat")
      .field(strField("name").build())
      .build()

    val animalType = newObject()
      .name("animal")
      .field(strField("name").build())
      .field(field("habitat", habitatType).dataFetcher(habitatFetcher).build())
      .build()


    val queryType1 = newObject()
      .name("query")
      .field(field("person", new GraphQLList(personType))
        .dataFetcher(personFetcher)
        .argument(intArg("code")).build())
      .build()

    val queryType2 = newObject()
      .name("query")
      .field(field("animal", new GraphQLList(animalType))
        .dataFetcher(animalFetcher).build())
      .build()

    val schema1 = GraphQLSchema.newSchema()
      .query(queryType1)
      .build()
    val schema2 = GraphQLSchema.newSchema()
      .query(queryType2)
      .build()

    def mapOfRootsToSchema(s:GraphQLSchema):Map[String,GraphQLSchema] = {
      s.getQueryType.getFieldDefinitions.map { f =>
        (f.getName -> s)
      }.toMap
    }
    mapOfRootsToSchema(schema1) ++ mapOfRootsToSchema(schema2)
  }

}
